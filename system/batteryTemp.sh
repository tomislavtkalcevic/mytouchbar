bat=$(/usr/local/bin/istats battery temp --value-only | xargs)
echo "BATTERY"
temp=${bat%.*}
if [ $temp -ge 40 -a $temp -lt 45 ]; then
    printf "\\e[33m$temp \xc2\xb0C"
elif [ $temp -ge 45 ]; then
    printf "\\e[31m$temp \xc2\xb0C"
else
    printf "\\e[32m$temp \xc2\xb0C"
fi;