fanspeeds=$(/usr/local/bin/istats fan speed --value-only | xargs)
fanA=$(echo $fanspeeds | awk '{print $1}')
fanB=$(echo $fanspeeds | awk '{print $2}')
##echo "$fanA RPM"
##echo "$fanB RPM"


if [ $fanA -ge 2000 -a $fanA -lt 2500 ]; then
    printf "\\e[33m$fanA RPM\e[0m\n"
elif [ $fanA -ge 2500 ]; then
    printf "\\e[31m$fanA RPM\e[0m\n"
else
    printf "\\e[32m$fanA RPM\e[0m\n"
fi;

if [ $fanB -ge 2000 -a $fanB -lt 2500 ]; then
    printf "\\e[33m$fanB RPM"
elif [ $fanB -ge 2500 ]; then
    printf "\\e[31m$fanB RPM"
else
    printf "\\e[32m$fanB RPM"
fi;