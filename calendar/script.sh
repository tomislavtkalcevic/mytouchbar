event=$(exec icalBuddy -n -npn -nc -ea -iep "title,datetime" -ps "|=|" -po "datetime,title"   -tf "=%H:%M" -df "" -eed eventsToday+ | head -n 1)
time=$(exec echo $event | awk -F "=" '{print substr($2,0,5)}')
name=$(exec echo $event | awk -F "=" '{print $3}')

printf "c: $time \n$name"
