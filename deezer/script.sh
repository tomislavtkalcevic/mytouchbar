deezer=$(exec tail -n 100 /Users/tomislav/Library/Logs/Deezer/log.log | grep "Emit current-track-changed" | tail -1 | cut -d "{" -f2  | cut -d "}" -f1)
playStatus=$(exec tail -n 100 /Users/tomislav/Library/Logs/Deezer/log.log | grep "Emit playing-changed" | tail -1 | cut -d " " -f6  | cut -d " " -f1)
json=$(echo { $deezer } )

title=$(echo $json | /usr/local/bin/jq .title | sed -e 's/^"//' -e 's/"$//')
author=$(echo $json | /usr/local/bin/jq .artist | sed -e 's/^"//' -e 's/"$//')
cover=$(echo $json | /usr/local/bin/jq .cover | sed -e 's/^"//' -e 's/"$//')

oldCover=$(cat oldCoverUrl)

#if [[ $oldCover != $cover ]]; then
#    coverDownlaod=$(exec curl $cover -o cover.jpg -s)
#    echo $cover > oldCoverUrl
#    echo 1 > updateCover
#fi

printf ' '
if [[ $playStatus == "false" ]]; then
    printf '(paused) '
fi
echo "$author - $title"
