ipaddress=$(ifconfig en0 | grep inet | grep -v inet6 | awk '{print $2}' | sed -e 's/^[[:space:]]*//')
wifi=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | awk '/ SSID/ {print substr($0, index($0, $2))}' | sed -e 's/^[[:space:]]*//')


if [ ! -z $ipaddress ]; then
    printf "WiFi: \\e[94m$wifi\\e[0m\n\\e[94m$ipaddress"
fi
