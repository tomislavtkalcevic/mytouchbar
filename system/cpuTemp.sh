cpuTemp=$(/usr/local/bin/istats cpu --value-only | xargs)
temp=${cpuTemp%.*}
echo "CPU"
if [ $temp -ge 55 -a $temp -lt 65 ]; then
    printf "\\e[33m$temp \xc2\xb0C"
elif [ $temp -ge 65 ]; then
    printf "\\e[31m$temp \xc2\xb0C"
else
    printf "\\e[32m$temp \xc2\xb0C"
fi;
